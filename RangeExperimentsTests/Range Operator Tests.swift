//
//  Range Operator Tests.swift
//  RangeExperimentsTests
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest
@testable import RangeExperiments

class RangeOperatorTests: XCTestCase {


	func test_unbounded_unbounded() {
		
	}
	
	func test_unbounded_included() {
		let r = …5
		XCTAssertEqual(r.lowerBound, .unbounded)
		XCTAssertEqual(r.upperBound, .included(5))
	}
	
	func test_unbonuded_excluded() {
		let r = …<5
		XCTAssertEqual(r.lowerBound, .unbounded)
		XCTAssertEqual(r.upperBound, .excluded(5))
	}
	
	func test_included_unbounded() {
		let r = 1…
		XCTAssertEqual(r.lowerBound, .included(1))
		XCTAssertEqual(r.upperBound, .unbounded)
	}
	
	func test_excluded_unbounded() {
		let r = 1<…
		XCTAssertEqual(r.lowerBound, .excluded(1))
		XCTAssertEqual(r.upperBound, .unbounded)
	}
	
	func test_included_included() {
		let r = 1…5
		XCTAssertEqual(r.lowerBound, .included(1))
		XCTAssertEqual(r.upperBound, .included(5))
	}
	
	func test_included_excluded() {
		let r = 1…<5
		XCTAssertEqual(r.lowerBound, .included(1))
		XCTAssertEqual(r.upperBound, .excluded(5))
	}
	
	func test_excluded_included() {
		let r = 1<…5
		XCTAssertEqual(r.lowerBound, .excluded(1))
		XCTAssertEqual(r.upperBound, .included(5))
	}
	
	func test_excluded_excluded() {
		let r = 1<…<5
		XCTAssertEqual(r.lowerBound, .excluded(1))
		XCTAssertEqual(r.upperBound, .excluded(5))
	}

}
