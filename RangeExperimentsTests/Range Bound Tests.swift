//
//  Range Bound Tests.swift
//  RangeExperimentsTests
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest
@testable import RangeExperiments


class RangeBoundTests: XCTestCase {

	func testUnboundedBound() {
		let b = RangeBound<Int>.unbounded
		XCTAssertEqual(b.isBounded, false)
		// XCTAssertEqual(b.isBoundInclusive, ???) // TODO: figure out a good behaviour for this
		XCTAssertNil(b.bound)
		
		XCTAssertEqual(b.isLowerThan(Int.min), true)
		XCTAssertEqual(b.isLowerThan(Int.max), true)
		XCTAssertEqual(b.isHigherThan(Int.min), true)
		XCTAssertEqual(b.isHigherThan(Int.max), true)
	}
	
	func testInclusiveBound() {
		let b = RangeBound<Int>.included(5)
		XCTAssertEqual(b.isBounded, true)
		XCTAssertEqual(b.isBoundInclusive, true)
		XCTAssertEqual(b.bound, 5)
		
		XCTAssertEqual(b.isLowerThan(0), false)
		XCTAssertEqual(b.isHigherThan(0), true)
		
		XCTAssertEqual(b.isLowerThan(5), true)
		XCTAssertEqual(b.isHigherThan(5), true)
		
		XCTAssertEqual(b.isLowerThan(10), true)
		XCTAssertEqual(b.isHigherThan(10), false)
	}
	
	func testExclusiveBound() {
		let b = RangeBound<Int>.excluded(5)
		XCTAssertEqual(b.isBounded, true)
		XCTAssertEqual(b.isBoundInclusive, false)
		XCTAssertEqual(b.bound, 5)
		
		XCTAssertEqual(b.isLowerThan(0), false)
		XCTAssertEqual(b.isHigherThan(0), true)
		
		XCTAssertEqual(b.isLowerThan(5), false)
		XCTAssertEqual(b.isHigherThan(5), false)
		
		XCTAssertEqual(b.isLowerThan(10), true)
		XCTAssertEqual(b.isHigherThan(10), false)
	}
}
