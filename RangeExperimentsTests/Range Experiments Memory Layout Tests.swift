//
//  Range Experiments Memory Layout Tests.swift
//  RangeExperimentsTests
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest
@testable import RangeExperiments

fileprivate extension MemoryLayout {
	static func printDescription() {
		print("\(T.self) takes up \(self.size) bytes, aligned to \(self.alignment) bytes, with a stride of \(self.stride) bytes")
	}
	
	static func assertEqualTo(size: Int, alignment: Int, stride: Int) {
		XCTAssertEqual(self.size, size)
		XCTAssertEqual(self.alignment, alignment)
		XCTAssertEqual(self.stride, stride)
	}
}

// These tests are only valid for x64
class Range_Experiments_Memory_Layout_Tests: XCTestCase {
	
	func testSimpleAnyRangeMemoryLayout() {
		let l = MemoryLayout<SimpleAnyRange<Int>>.self
		l.printDescription()
		l.assertEqualTo(size: 25, alignment: 8, stride: 32)
	}
	
	func testPackedRangeMemoryLayout() {
		let l = MemoryLayout<PackedAnyRange<Int>>.self
		l.printDescription()
		l.assertEqualTo(size: 17, alignment: 8, stride: 24)
	}

}
