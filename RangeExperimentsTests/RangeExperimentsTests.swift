//
//  RangeExperimentsTests.swift
//  RangeExperimentsTests
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import XCTest
@testable import RangeExperiments

class RangeExperimentsTests: XCTestCase {

	private func assert<R: BoundedRange>(that haystack: R, contains needle: Int, is expected: Bool)
		where R.Bound == Int {
		let actual = haystack.contains(needle)
		
		switch (actual, expected) {
		case (false, false): break
		case ( true,  true): break
			
		case (false,  true):
			XCTFail("\(haystack) is supposed to contain \(needle), but it didn't.")
		case ( true, false):
			XCTFail("\(haystack) is not supposed to contain \(needle), but it did")
		}
	}
	
	func test_unbounded_unbounded() {
		
	}
	
	func test_unbounded_included() {
		assert(that: …5, contains: 4, is: true )
		assert(that: …5, contains: 5, is: true )
		assert(that: …5, contains: 6, is: false)
	}
	
	func test_unbonuded_excluded() {
		assert(that: …<5, contains: 4, is: true )
		assert(that: …<5, contains: 5, is: false)
		assert(that: …<5, contains: 6, is: false)
	}
	
	func test_included_unbounded() {
		assert(that: 1…, contains: 0, is: false)
		assert(that: 1…, contains: 1, is: true )
		assert(that: 1…, contains: 2, is: true )
	}
	
	func test_excluded_unbounded() {
		assert(that: 1<…, contains: 0, is: false)
		assert(that: 1<…, contains: 1, is: false)
		assert(that: 1<…, contains: 2, is: true )
	}
	
	func test_included_included() {
		assert(that: 1…5, contains: 0, is: false)
		assert(that: 1…5, contains: 1, is: true )
		assert(that: 1…5, contains: 2, is: true )
		assert(that: 1…5, contains: 3, is: true )
		assert(that: 1…5, contains: 4, is: true )
		assert(that: 1…5, contains: 5, is: true )
		assert(that: 1…5, contains: 6, is: false)
	}
	
	func test_included_excluded() {
		assert(that: 1…<5, contains: 0, is: false)
		assert(that: 1…<5, contains: 1, is: true )
		assert(that: 1…<5, contains: 2, is: true )
		assert(that: 1…<5, contains: 3, is: true )
		assert(that: 1…<5, contains: 4, is: true )
		assert(that: 1…<5, contains: 5, is: false)
		assert(that: 1…<5, contains: 6, is: false)
	}
	
	func test_excluded_included() {
		assert(that: 1<…5, contains: 0, is: false)
		assert(that: 1<…5, contains: 1, is: false)
		assert(that: 1<…5, contains: 2, is: true )
		assert(that: 1<…5, contains: 3, is: true )
		assert(that: 1<…5, contains: 4, is: true )
		assert(that: 1<…5, contains: 5, is: true )
		assert(that: 1<…5, contains: 6, is: false)
	}
	
	func test_excluded_excluded() {
		assert(that: 1<…<5, contains: 0, is: false)
		assert(that: 1<…<5, contains: 1, is: false)
		assert(that: 1<…<5, contains: 2, is: true )
		assert(that: 1<…<5, contains: 3, is: true )
		assert(that: 1<…<5, contains: 4, is: true )
		assert(that: 1<…<5, contains: 5, is: false)
		assert(that: 1<…<5, contains: 6, is: false)
	}

	
	func testAlternateTestImplementation() {
		let (T, F) = (true, false)
		
		func test<R: BoundedRange>(range: R, input inputs: [Int],
								   expectedOutput expectedOutputs: [Bool])
		where R.Bound == Int {
			for (input, expectedOutput) in zip(inputs, expectedOutputs) {
				assert(that: range, contains: input, is: expectedOutput)
			}
		}
		
		test(range: …5, input: [4, 5, 6],
			 expectedOutput:   [T, T, F])
		
		test(range: …<5, input: [4, 5, 6],
			 expectedOutput:    [T, F, F])
		
		test(range: 1…, input: [0, 1, 2],
			 expectedOutput:   [F, T, T])
		
		test(range: 1<…, input: [0, 1, 2],
			 expectedOutput:    [F, F, T])
		
		test(range: 1…5, input: [0, 1, 2, 3, 4, 5, 6],
			 expectedOutput:    [F, T, T, T, T, T, F])
		
		test(range: 1…<5, input: [0, 1, 2, 3, 4, 5, 6],
			 expectedOutput:     [F, T, T, T, T, F, F])
		
		test(range: 1<…5, input: [0, 1, 2, 3, 4, 5, 6],
			 expectedOutput:     [F, F, T, T, T, T, F])
		
		test(range: 1<…<5, input: [0, 1, 2, 3, 4, 5, 6],
			 expectedOutput:      [F, F, T, T, T, F, F])
	}
	
}
