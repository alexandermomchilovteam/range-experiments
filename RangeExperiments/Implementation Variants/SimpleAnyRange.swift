//
//  SimpleAnyRange.swift
//  RangeExperiments
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

/// An all-encompassing range type, with a lower bound and upper bount that could each be unbounded
/// (i.e. don't exist), or bounded (either including or excluding the boundary value itself)
public struct SimpleAnyRange<T: Comparable>: BoundedRange {
	
	public let lowerBound: RangeBound<T>
	public let upperBound: RangeBound<T>
	
	public init(lowerBound: RangeBound<T>, upperBound: RangeBound<T>) {
		self.lowerBound = lowerBound
		self.upperBound = upperBound
	}
	
	public func contains(_ value: T) -> Bool {
		return self.lowerBound.isLowerThan(value) && self.upperBound.isHigherThan(value)
	}
}

extension SimpleAnyRange: Equatable where T: Equatable {}
extension SimpleAnyRange: Hashable where T: Hashable {}
