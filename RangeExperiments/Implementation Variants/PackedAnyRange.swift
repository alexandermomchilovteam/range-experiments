//
//  PackedAnyRange.swift
//  RangeExperiments
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

/// An all-encompassing range type, similar to SimpleAnyRange, but using an enum as the sole
/// backing storage.
///
/// While the implementation is much more tideious, the compiler packs the enum
/// members more efficiently, resulting in a 24 byte stride, rather than 32.
///
/// This is a wrapper for the _PackedAnyRange, but hides the implementation details of
/// the enum cases.
public struct PackedAnyRange<T: Comparable>: BoundedRange {

	private let wrapped: _PackedAnyRange<T>
	
	public init(lowerBound: RangeBound<T>, upperBound: RangeBound<T>) {
		self.wrapped = _PackedAnyRange(lowerBound: lowerBound, upperBound: upperBound)
	}
	
	public var lowerBound: RangeBound<T> { return self.wrapped.lowerBound }
	public var upperBound: RangeBound<T> { return self.wrapped.upperBound }
	
	public func contains(_ value: T) -> Bool {
		return wrapped.contains(value)
	}
}

extension PackedAnyRange: Equatable where T: Equatable {}
extension PackedAnyRange: Hashable where T: Hashable {}

private enum _PackedAnyRange<T: Comparable> {
	// I'm aware lowerCamelCase is the convetion for enum case names, but the underscores *really* improve clarity.
	// It doesn't matter too much, because this is a private type, exposed publically through PackedAnyRange
	case unbounded_unbounded
	case unbounded_included(upperBound: T)
	case unbonuded_excluded(upperBound: T)
	
	case included_unbounded(lowerBound: T)
	case included_included(lowerBound: T, upperBound: T)
	case included_excluded(lowerBound: T, upperBound: T)
	
	case excluded_unbounded(lowerBound: T)
	case excluded_included(lowerBound: T, upperBound: T)
	case excluded_excluded(lowerBound: T, upperBound: T)
}

extension _PackedAnyRange: Equatable where T: Equatable {}
extension _PackedAnyRange: Hashable where T: Hashable {}

extension _PackedAnyRange: BoundedRange {
	
	public init(lowerBound: RangeBound<T>, upperBound: RangeBound<T>) {
		switch (lowerBound, upperBound) {
		case     (.unbounded, .unbounded):
			self = .unbounded_unbounded
		case let (.unbounded, .included(ub)):
			self = .unbounded_included(upperBound: ub)
		case let (.unbounded, .excluded(ub)):
			self = .unbonuded_excluded(upperBound: ub)
			
		case let (.included(lb), .unbounded):
			self = .included_unbounded(lowerBound: lb)
		case let (.included(lb), .included(ub)):
			self = .included_included(lowerBound: lb, upperBound: ub)
		case let (.included(lb), .excluded(ub)):
			self = .included_excluded(lowerBound: lb, upperBound: ub)
			
		case let (.excluded(lb), .unbounded):
			self = .excluded_unbounded(lowerBound: lb)
		case let (.excluded(lb), .included(ub)):
			self = .excluded_included(lowerBound: lb, upperBound: ub)
		case let (.excluded(lb), .excluded(ub)):
			self = .excluded_excluded(lowerBound: lb, upperBound: ub)
		}
	}
	
	
	public var lowerBound: RangeBound<T> {
		switch self {
		case     .unbounded_unbounded:      return .unbounded
		case     .unbounded_included(_):    return .unbounded
		case     .unbonuded_excluded(_):    return .unbounded
			
		case let .included_unbounded(lb):   return .included(lb)
		case let .included_included(lb, _): return .included(lb)
		case let .included_excluded(lb, _): return .included(lb)
			
		case let .excluded_unbounded(lb):   return .excluded(lb)
		case let .excluded_included(lb, _): return .excluded(lb)
		case let .excluded_excluded(lb, _): return .excluded(lb)
		}
	}
	
	public var upperBound: RangeBound<T> {
		switch self {
		case     .unbounded_unbounded:      return .unbounded
		case let .unbounded_included(ub):   return .included(ub)
		case let .unbonuded_excluded(ub):   return .excluded(ub)
			
		case     .included_unbounded(_):    return .unbounded
		case let .included_included(_, ub): return .included(ub)
		case let .included_excluded(_, ub): return .excluded(ub)
			
		case     .excluded_unbounded(_):    return .unbounded
		case let .excluded_included(_, ub): return .included(ub)
		case let .excluded_excluded(_, ub): return .excluded(ub)
		}
	}
	
	public func contains(_ v: T) -> Bool {
		switch self {
		case .unbounded_unbounded:
			return true
		case .unbounded_included(let upperBound):
			return v <= upperBound
		case .unbonuded_excluded(let upperBound):
			return v <  upperBound
			
		case .included_unbounded(let lowerBound):
			return lowerBound <= v
		case .included_included(let lowerBound, let upperBound):
			return lowerBound <= v && v <= upperBound
		case .included_excluded(let lowerBound, let upperBound):
			return lowerBound <= v && v <  upperBound
			
		case .excluded_unbounded(let lowerBound):
			return lowerBound < v
		case .excluded_included(let lowerBound, let upperBound):
			return lowerBound < v && v <= upperBound
		case .excluded_excluded(let lowerBound, let upperBound):
			return lowerBound < v && v <  upperBound
		}
	}
}

// These APIs are verbose to implement, but skip the "middleman" of RangeBound.
// Probably not worth having, but I added for comparison/consideration.
extension _PackedAnyRange {
	// Added for completeness, but the `self.lowerBound: RangeBound<T>` API is probably better
	var lowerBoundValue: T? {
		switch self {
		case .included_unbounded(let lowerBound),
			 .included_included(let lowerBound, _),
			 .included_excluded(let lowerBound, _),
			 .excluded_unbounded(let lowerBound),
			 .excluded_included(let lowerBound, _),
			 .excluded_excluded(let lowerBound, _): return lowerBound
		default: return nil
		}
	}
	
	// Added for completeness, but the `self.upperBound: RangeBound<T>` API is probably better
	var upperBoundValue: T? {
		switch self {
		case .unbounded_included(let upperBound),
			 .unbonuded_excluded(let upperBound),
			 .included_included(_, let upperBound),
			 .included_excluded(_, let upperBound),
			 .excluded_included(_, let upperBound),
			 .excluded_excluded(_, let upperBound): return upperBound
		default: return nil
		}
	}
	
	// Added for completeness, but the `self.lowerBound: RangeBound<T>` API is probably better
	var lowerBoundIsInclusive: Bool {
		switch self {
		case .included_unbounded(_),
			 .included_included(_, _),
			 .included_excluded(_, _): return true
		default: return false
		}
	}
	
	// Added for completeness, but the `self.upperBound: RangeBound<T>` API is probably better
	var upperBoundIsInclusive: Bool {
		switch self {
		case .unbounded_included(_),
			 .included_included(_, _),
			 .excluded_included(_, _): return true
		default: return false
		}
	}
}
