//
//  Bridging from Swift Native Range Types.swift
//  RangeExperiments
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

extension SimpleAnyRange {
	// a..<b
	init(_ r: Range<T>) {
		self.init(
			lowerBound: .included(r.lowerBound),
			upperBound: .excluded(r.upperBound)
		)
	}
	
	// a...b
	init(_ r: ClosedRange<T>) {
		self.init(
			lowerBound: .included(r.lowerBound),
			upperBound: .included(r.upperBound)
		)
	}
	
	// ...b
	init(_ r: PartialRangeThrough<T>) {
		self.init(
			lowerBound: .unbounded,
			upperBound: .included(r.upperBound)
		)
	}
	
	// ..<b
	init(_ r: PartialRangeUpTo<T>) {
		self.init(
			lowerBound: .unbounded,
			upperBound: .excluded(r.upperBound)
		)
	}
	
	// a...
	init(_ r: PartialRangeFrom<T>) {
		self.init(
			lowerBound: .included(r.lowerBound),
			upperBound: .unbounded
		)
	}
	
	// ...
	init(_: UnboundedRange) {
		self.init(lowerBound: .unbounded, upperBound: .unbounded)
	}
}
