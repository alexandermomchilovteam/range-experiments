//
//  Custom Range Operators.swift
//  RangeExperiments
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

public typealias RangeOperatorResultType = PackedAnyRange


//infix operator …
//let … = AnyRange(lowerBound: .unbounded, upperBound: .unbounded)

prefix operator …
public prefix func … <T>(bound: T) -> RangeOperatorResultType<T> { // …a
	return RangeOperatorResultType(lowerBound: .unbounded, upperBound: .included(bound))
}

prefix operator …<
public prefix func …< <T>(bound: T) -> RangeOperatorResultType<T> { // …<a
	return RangeOperatorResultType(lowerBound: .unbounded, upperBound: .excluded(bound))
}


postfix operator …
public postfix func … <T>(bound: T) -> RangeOperatorResultType<T> { // a…
	return RangeOperatorResultType(lowerBound: .included(bound), upperBound: .unbounded)
}

postfix operator <…
public postfix func <… <T>(bound: T) -> RangeOperatorResultType<T> { // a<…
	return RangeOperatorResultType(lowerBound: .excluded(bound), upperBound: .unbounded)
}


infix operator …
public func … <T>(lhs: T, rhs: T) -> RangeOperatorResultType<T> { // a…b
	return RangeOperatorResultType(lowerBound: .included(lhs), upperBound: .included(rhs))
}

infix operator …<
public func …< <T>(lhs: T, rhs: T) -> RangeOperatorResultType<T> { // a…<b
	return RangeOperatorResultType(lowerBound: .included(lhs), upperBound: .excluded(rhs))
}

infix operator <…
public func <… <T>(lhs: T, rhs: T) -> RangeOperatorResultType<T> { // a<…b
	return RangeOperatorResultType(lowerBound: .excluded(lhs), upperBound: .included(rhs))
}

infix operator <…<
public func <…< <T>(lhs: T, rhs: T) -> RangeOperatorResultType<T> { // a<…<b
	return RangeOperatorResultType(lowerBound: .excluded(lhs), upperBound: .excluded(rhs))
}
