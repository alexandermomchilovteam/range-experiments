//
//  RangeBound.swift
//  RangeExperiments
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

/// An API for modelling one bound of a range
///
/// This is similar to Optional (where unbounded is like `nil`), but the `some` case has two
/// counter parts: one where the boundary value is included, and one where it is not.
public enum RangeBound<T: Comparable> {
	case unbounded
	case included(T)
	case excluded(T)
	
	public func isLowerThan(_ x: T) -> Bool {
		switch self {
		case .unbounded: return true
		case .included(let bound): return bound <= x
		case .excluded(let bound): return bound <  x
		}
	}
	
	public func isHigherThan(_ x: T) -> Bool {
		switch self {
		case .unbounded: return true
		case .included(let bound): return x <= bound
		case .excluded(let bound): return x <  bound
		}
	}
}

extension RangeBound: Equatable {}
extension RangeBound: Hashable where T: Hashable {}

// So convenience properties for inspecting the bound. Alternatively, these could be implemented on the range
// but that ends up much longer, with more repetition, and overall feels like an inappropraite place
// for this code. I doubt that manual "inlining" would have any perforamcne benefit at all.
public extension RangeBound {
	var isBounded: Bool {
		return self.bound != nil
	}
	
	var isBoundInclusive: Bool {
		switch self {
		case .unbounded: fatalError() // What's a good way to design this case in the API?
		case .included: return true
		case .excluded: return false
		}
	}
	
	var bound: T? {
		switch self {
		case .unbounded: return nil
		case .included(let bound), .excluded(let bound): return bound
		}
	}
}
