//
//  BoundedRange.swift
//  RangeExperiments
//
//  Created by Alexander Momchilov on 2019-08-27.
//  Copyright © 2019 Alexander Momchilov. All rights reserved.
//

import Foundation

/// A protocol that captures the interface of the multiple experimental implementaitons,
/// so that code can be written to agnostically use any one of them.
public protocol BoundedRange {
	associatedtype Bound: Comparable
	
	init(lowerBound: RangeBound<Bound>, upperBound: RangeBound<Bound>)
	
	var lowerBound: RangeBound<Bound> { get }
	var upperBound: RangeBound<Bound> { get }
	
	func contains(_: Bound) -> Bool
}
